# cafeteria

Main repository with submodules

## Install
- git clone --recurse-submodules https://gitlab.com/cafeteria-bc/cafeteria.git
- Optional: Change ports in docker-compose.override.yml
- docker-compose up
- docker-compose exec api php artisan migrate --seed
- Open: http://127.0.0.1:4000/